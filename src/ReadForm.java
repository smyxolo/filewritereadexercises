import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class ReadForm {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        HashMap<String, Form> dataBase = new HashMap<>();
        Boolean isWorking = true;

        System.out.println("Wpisz nazwe pliku do odczytu:");
        String inputLine = input.nextLine();

        if (loadFile(inputLine,dataBase)) {
            while (isWorking){
                checkRecords(input, dataBase, isWorking);
            }
        }
        System.out.println(dataBase);

    }

    public static void checkRecords(Scanner input, HashMap<String, Form> dataBase, boolean isWorking){
        System.out.println("Podaj imie i nazwisko, aby wyświetlić dane lub wpisz 'quit', aby wyjść: ");
        String inputLine = input.nextLine();
        if(inputLine.equals("quit")){
            isWorking = false;
        }
        else if(dataBase.containsKey(inputLine)){
            System.out.println(dataBase.get(inputLine));
        }
        else System.out.println("Tej osoby nie ma w bazie danych.");
    }

    public static boolean loadFile(String inputLine, HashMap<String, Form> dataBase){
        File file = new File(inputLine);
        try(Scanner fileScan = new Scanner(file)) {
            readContents(fileScan, dataBase);
            System.out.println("Plik wczytany.");
            return true;
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println("Plik nie istnieje.");
            return false;
        }
    }

    public static void readContents(Scanner fileScan, HashMap<String, Form> dataBase){
        Form form = new Form();
        while (fileScan.hasNextLine()){
            String line = fileScan.nextLine();

            if(line.contains("Imie i nazwisko:")){
                String name = line.split(":", 2)[1].trim();
                dataBase.put(name, form);
                form = dataBase.get(name);
                form.setImieNazwisko(name);
            }
            else if(line.contains("Wiek:")){
                Integer wiek = Integer.parseInt(line.split(":", 2)[1].trim());
                form.setWiek(wiek);
            }
            else if(line.contains("Plec:")){
                Plec plec = Plec.valueOf(line.split(":", 2)[1].trim());
                form.setPlec(plec);
            }
            else if(line.contains("Waga:")){
                Integer waga = Integer.parseInt(line.split(":", 2)[1].trim());
                form.setWaga(waga);
                form.setHasAdditionalQuestions(true);
            }
            else if(line.contains("Kolor wlosow:")){
                String kolorWlosow = line.split(":", 2)[1].trim();
                form.setKolorWlosow(kolorWlosow);
            }
            else if(line.contains("Kolor oczu:")){
                String kolorOczu = line.split(":", 2)[1].trim();
                form.setKolorOczu(kolorOczu);
            }
            else if(!line.contains("separator")){
                System.out.println("Wrong file format");
            }
        }
    }
}
