import java.io.File;
import java.util.Scanner;

public class FileProperties {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        String inputLine = "";
        boolean isWorking = true;

        while (isWorking){
            inputLine = skan.nextLine();
            if(inputLine.equals("quit")){
                isWorking = false;
            }
            File plik = new File(inputLine);
            System.out.println("File exists: " + plik.exists());
            System.out.println("Relative path: " + plik.getPath());
            System.out.println("Absolute path: " + plik.getAbsolutePath());
            System.out.println("File size: " + plik.length());
            System.out.println("Last modified: " + plik.lastModified());
            System.out.println("Hidden property: " + plik.isHidden());
            System.out.println("Read access: " + plik.canRead());
            System.out.println("Write access: " + plik.canWrite());
            System.out.println("Executable: " + plik.canExecute());
            System.out.println("Is directory: " + plik.isDirectory());
            if(plik.isDirectory()){
                try {
                    for (File podplik : plik.listFiles()) {
                        System.out.println(podplik);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
