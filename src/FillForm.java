import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;

public class FillForm {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Boolean isWorking = true;
        HashMap<String, Form> formEntriesToSave = new HashMap<>();
        while (isWorking){
            pytania(scan, formEntriesToSave);
            System.out.println("Wpisz 'quit', aby zakończyć lub wciśnij Enter aby kontynuować.");
            if(scan.nextLine().toLowerCase().contains("quit")) isWorking = false;
        }
        printToFile(formEntriesToSave, scan);
    }


    public static void pytania(Scanner scan, HashMap<String, Form> formList){
        Form form = new Form();
        System.out.println("Podaj imie i nazwisko: ");
        form.setImieNazwisko(scan.nextLine());
        System.out.println("Podaj swoj wiek: ");
        try {
            Integer wiek = Integer.parseInt(scan.nextLine());
            form.setWiek(wiek);
        }catch (NumberFormatException nfe){
            System.out.println("Zly format wieku. Podaj liczbe.");
            System.out.println("Sprobuj ponownie.");
        }
        System.out.println("Podaj swoja plec: ");
        try {
            Plec plec = Plec.valueOf(scan.nextLine().toUpperCase());
            form.setPlec(plec);
        }catch (IllegalArgumentException iae){
            System.out.println("Zly format plci. Wpisz kobieta/mezczyzna");
            System.out.println("Sproboj ponownie.");
        }
        if((form.getPlec().equals(Plec.KOBIETA)&&(form.getWiek()>=18 && form.getWiek()<=25))||
                ((form.getPlec().equals(Plec.MEZCZYZNA))&&(form.getWiek()>=25&&form.getWiek()<=30))){
            form.setHasAdditionalQuestions(true);
            pytaniaDodatkowe(scan, form);
        }
        formList.put(form.getImieNazwisko(), form);

    }

    public static void pytaniaDodatkowe(Scanner scan, Form form){
        System.out.println("Podaj swoja wage: ");
        try {
            form.setWaga(Integer.parseInt(scan.nextLine()));
        }catch (NumberFormatException nfe){
            System.out.println("Nieprawidlowy format wagi. Sprobuj jeszcze raz wpisujac liczbe.");
        }
        System.out.println("Podaj kolor wlosow: ");
        form.setKolorWlosow(scan.nextLine());
        System.out.println("Podaj kolor oczu: ");
        form.setKolorOczu(scan.nextLine());
    }

    public static void printToFile(HashMap<String, Form> formsToSave, Scanner scan){
        System.out.println("Podaj nazwe pliku do jakiego chcesz zapisac dane: ");
        try(PrintWriter pw = new PrintWriter(new FileWriter(scan.nextLine(), true))) {
            for (Form form : formsToSave.values()) {
                pw.println("Imie i nazwisko: " + form.getImieNazwisko());
                pw.println("Wiek: " + form.getWiek());
                pw.println("Plec: " + form.getPlec());
                if(form.getHasAdditionalQuestions()){
                    pw.println("Waga: " + form.getWaga());
                    pw.println("Kolor wlosow: " + form.getKolorWlosow());
                    pw.println("Kolor oczu: " + form.getKolorOczu());
                }
                pw.println("-separator-");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
