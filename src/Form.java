public class Form {
    private String imieNazwisko;
    private Plec plec;
    private Integer wiek;
    private Boolean hasAdditionalQuestions = false;
    private Integer waga;
    private String kolorWlosow;
    private String kolorOczu;

    public Integer getWaga() {
        return waga;
    }

    public String getKolorWlosow() {
        return kolorWlosow;
    }

    public String getKolorOczu() {
        return kolorOczu;
    }

    public void setWaga(Integer waga) {
        this.waga = waga;
    }

    public void setKolorWlosow(String kolorWlosow) {
        this.kolorWlosow = kolorWlosow;
    }

    public void setKolorOczu(String kolorOczu) {
        this.kolorOczu = kolorOczu;
    }

    public Boolean getHasAdditionalQuestions() {
        return hasAdditionalQuestions;
    }

    public void setHasAdditionalQuestions(Boolean hasAdditionalQuestions) {
        this.hasAdditionalQuestions = hasAdditionalQuestions;
    }

    public void setImieNazwisko(String imieNazwisko) {
        this.imieNazwisko = imieNazwisko;
    }

    public void setPlec(Plec plec) {
        this.plec = plec;
    }

    public void setWiek(Integer wiek) {
        this.wiek = wiek;
    }

    public String getImieNazwisko() {
        return imieNazwisko;
    }

    public Plec getPlec() {
        return plec;
    }

    public Integer getWiek() {
        return wiek;
    }

    @Override
    public String toString() {
        return "Form{" +
                "imieNazwisko='" + imieNazwisko + '\'' +
                ", plec=" + plec +
                ", wiek=" + wiek +
                ", waga=" + waga +
                ", kolorWlosow='" + kolorWlosow + '\'' +
                ", kolorOczu='" + kolorOczu + '\'' +
                '}';
    }
}
