Files
Java exercises in file reading and writing

Zadanie   1:
Należy   napisać   aplikację   (wykorzystując   załączony   kod)   która   w   pętli   while   czyta   ze Scannera   wejście   użytkownika   z   konsoli,   a   następnie   linia   po   linii   wypisuje   tekst   do   pliku. Aplikacja   ma   się   zamykać   po   wpisaniu   przez   użytkownika   komendy   "quit".
Zadanie   2:
Rozwijamy   dotychczasową   aplikację.   Teraz   -   oczekujemy   od   użytkownika   konkretnych danych,   które   będą   zapisywane   do   pliku.   Aplikacja   będzie   służyć   jako   formularz   który   zbiera dane   (jak   z   ankiety).   Zakładamy   ankietę   użytkownika   która   pyta   użytkownika   o   następujące dane:
Podaj   imie   i   nazwisko: Podaj   wiek:
Podaj   płeć:
Kolejne   pytania   zadajemy   dopiero   wtedy,   gdy   użytkownik   jest: a)   kobietą   w   wieku   między   18-25
b)   mężczyzną   w   wieku   między   25-30
(powiedzmy   że   chodzi   o   jednakową   dojrzałość   osób   biorących   udział   w   ankiecie) Wymyśl   3   pytania   i   zadaj   je   ankietowanemu.
Wszystkie   pytania   i   odpowiedzi   ankiety   zapisz   do   pliku.
Otwieraj   plik   wyłącznie   do   dopisywania   danych   (jeśli   istnieje).
Rozszerzenie:
Stwórz   klasę   modelu   (Form)   która   przechowuje   odpowiedzi   na   wszystkie   pytania.   Dodaj metodę   toString,   która   będzie   używana   do   wypisania   obiektu   do   pliku. https://bitbucket.org/nordeagda2/simpleformapplication
Zadanie   3:
Stwórz   aplikację,   która   służy   do   przeglądania   katalogów.
Przyjmuj   w   pętli   linię   z   wejścia.   Po   odczytaniu   linii   przekaż   ją   do   obiektu   typu   File,   a następnie   wypisz   o   nim   kolejno   informacje:
Plik/katalog   istnieje:   Tak/Nie Ścieżka   relatywna:
Ścieżka   absolutna: Wielkość   pliku/katalogu: Ścieżka   absolutna:
Data   ostatniej   modyfikacji: Czy   plik   jest   ukryty:
 
 Prawo   dostępu   do   odczytu:
Prawo   dostępu   do   zapisu:
Prawo   dostępu   do   wykonywania(Executable):
(Jeśli   jest   katalogiem)
Lista   plików   w   tym   katalogu   (wraz   z   ich   datą   modyfikacji   oraz   rozmiarem   [oddzielone spacjami])
Rozszerzenie   zadania:
Po   napisaniu   tej   aplikacji,   obsłuż   komendy: delete   ŚCIEŻKA
create_folder   ŚCIEŻKA
create_file   ŚCIEŻKA
https://bitbucket.org/nordeagda2/simplefilecapabilities
Zadanie   4:
Stworzymy   aplikację,   która   służy   do   czytania   pliku   naszego   formularza.
Wcześniej   stworzyliśmy   aplikację,   która   zapisywała   nam   dane,   które   użytkownik   wpisał   w formularzu.   Była   to   aplikacja:   https://bitbucket.org/nordeagda2/simpleformapplication
Dane   z   tej   aplikacji   znalazły   się   w   pliku   test.txt.   eraz   wykorzystując   plik   'test.txt'   przejdziemy do   czytania   jego   zawartości,   a   następnie   tuż   po   przeczytaniu   analizujemy   wybraną linię/wynik   formularza.   Celem   aplikacji   jest   załadowanie   wszystkich   wpisów   z   pliku   do kolekcji   (proponuję   Mapę,   w   której   kluczem   jest   imie   i   nazwisko   osoby   biorącej   udział   w ankiecie,   a   wartością   jest   instancja   obiektu   Form   /   czy   też   instancja   klasy   reprezentującej obiekt   ankiety).
Następnym   krokiem   jest   stworzenie   odczytu   wejścia   użytkownika   (Scanner)   które   będzie czytać   linie   z   wejścia.   Zakładamy   że   użytkownik   będzie   wpisywał   linie   które   są   imieniem   i nazwiskiem   kolejnych   osób.   po   odczytaniu   linii   szukamy   w   naszej   kolekcji   osoby   o   tym imieniu   i   nazwisku,   a   po   znalezieniu   wypisujemy   informacje   o   tym   jak   dana   osoba   wypełniła ankietę.
Wskazówka   1    -   zweryfikuj   format   pliku.   Sprawdź   format   (kolejność   oraz   ich   składnie) danych.   Np.   Jeśli   zapisaliśmy   do   pliku:
Imie=pawel Wiek=23 Plec=M
to   możemy   założyć   że   podczas   odczytu   pobierając   z   pliku   linia   po   linii   możemy   się spodziewać   w   pierwszej   linii   imienia,   drugiej   wieku,   w   trzeciej   płci,   a   każda   linia   będzie   w formacie:
NAZWA_POLA=WARTOŚĆ

 czyli   wartość   będzie   oddzielona   od   nazwy   pola   znakiem   '='
Wskazówka2    -   po   przeczytaniu   tych   trzech   wartości   możemy   sprawdzić   czy   powinniśmy czytać   kolejne   3   (odpowiedzi   na   pytania)   czy   nie.
https://bitbucket.org/nordeagda2/simplefilereader
Zadanie   5   DODATKOWE:
TO   JEST   ZADANIE   DODATKOWE!
Stwórz   aplikację,   która   zapisuje   dane   w   formacie   binarnym.
Wykorzystamy   aplikację   z   formularzem.   Wczytaj   dane   w   taki   sam   sposób   jak   dane   czytane w   formularzu   z   zadania   https://bitbucket.org/nordeagda2/simpleformapplication/overview Po   odczytaniu   danych   z   formularza   posłużymy   się   klasą   DataOutputStream   do   zapisu danych   do   pliku.   Format   ma   wciąż   być   czytelny   (nie   dla   oka,   tylko   dla   nas   później),   dlatego każdy   wpis   będzie   wymagał   dodatkowych   informacji,   np.   długości   zmiennych   tekstowych, aby   określić   rozmiar   wpisu.
Wskazówka    -   przed   każdą   zmienną   typu   String   wstaw   4   bajty   (int)   określający   jak   długa będzie   ta   zmienna   (ten   String).
Zadanie   6:
Przygotuj   DWIE   aplikacje.   Celem   zadania   będzie   przekazanie   danych   z   jednej   aplikacji   do drugiej.   Ponieważ   jeszcze   nie   dotarliśmy   do   komunikacji   między   aplikacjami,   spróbujemy zrobić   to   w   "łopatologiczny"   sposób   -   czyli   posługując   się   własnym   mechanizmem   wymiany danych.
Chcemy   aby   jedna   z   aplikacji   pozwalała   na   pisanie   danych,   a   druga   odbierała   te   dane.   W tym   celu   stworzymy   plik   wymiany   danych.
---
Pierwsza   aplikacja   będzie   czytała   ze   Scanner'a   dane,   które   będzie   od   razu   zapisywała   do pliku.   Po   każdym   zapisaniu   linii   do   pliku   wypisz   na   konsolę   komunikat:
Przesłano:   XYZ
gdzie   XYZ   jest   zawartością   linii,   która   została   umieszczona   w   pliku.   Powtarzaj   wykonanie   tej czynności   w   pętli   while   -   dopóki   użytkownik   nie   wpisze   komendy   "quit".
Dodatkowo   -   dodaj   obsługę   dodatkowej   komendy: SET_FILE   nazwa_pliku
po   wpisaniu   takiej   linii   do   konsoli   aplikacja   powinna   otwierać   nowy   plik   o   nazwie nazwa_pliku.   Czyli   -   próbujemy   zaimplementować   funkcjonalność   przełączania   zapisu   do różnych   plików.
---
Druga   aplikacja   odpowiada   za   czytanie   pliku.
Po   odczytaniu   linii   z   pliku   aplikacja   będzie   przetwarzać   tą   linię.   Na   początku   zróbmy   proste

przetwarzanie   -   po   każdym   odczycie   wypisz   linię   na   ekran: Odebrano   linię:   XYZ
gdzie   XYZ   jest   linią   z   pliku.
Nie   czyść   pliku   po   odczycie.   Wykonuj   odczyty   tylko   gdy:
a)   zmieniła   się   wielkość   pliku   oraz   czas   ostatniej   modyfikacji b)   zmieniła   się   ostatnia   odczytana   linia   z   pliku.
Dodatkowo   -   spróbuj   dodać   opóźnienie   czytania   z   pliku.   Zwróć   uwagę   na   to,   że   po uruchomieniu   aplikacji   pętla   czytająca   plik   stale   wykonuje   odczyt   i   zużywa   bardzo   dużo zasobów   (spójrz   na   zużycie   procesora).   W   pętli   powinniśmy   dopisać   "odsypianie"   określoną ilość   czasu,   aby   nie   obciążać   tak   bardzo   procesora.   Możemy   to   zrobić,   ponieważ   nie   ma potrzeby   czytania   pliku   (w   zależności   od   procesora)   tysiące   razy   na   sekundę,   wystarczy   to zrobić   raz,   a   następnie   odczekać   chwilę.
Dodatkowo   -   podobnie   jak   w   pierwszej   aplikacji   zaimplementuj   obsługę   komendy: SET_FILE   nazwa_pliku
po   wpisaniu   tej   linii   do   konsoli   aplikacja   powinna   otwierać   nowy   plik   z   którego   następnie czyta   kolejne   linie.
ROZSZERZENIE:
Dodaj   funkcjonalność   czytania   wielu   plików. Do   obsługiwanych   komend   dodaj   komendę:
ADD_NEW_READER   FILE
który   powoduje,   że   READER(czyli   apka   czytająca)   dodaje   do   kolekcji   plików   czytanych (obecnie   czytalismy   tylko   jeden   plik)   nowy   plik   FILE.   Komenda   ma   przejść   transparentnie przez   WRITERA,   ale   w   READERZE   ma   być   interpretowana   i   pod   wpływem   odczytania   tej instrukcji   powinieneś   rozszerzyć   listę   czytanych   plików.
W   writerze   dodaj   funkcjonalność: po   odczytaniu   z   wejścia   komendy:
TOFILE   FILE   COMMAND
do   pliku   o   nazwie   FILE   zapisz   tekst   o   nazwie   COMMAND   (reszta   linii).   Tak,   aby
przetestować   funkcjonalność   READERA.
Zadanie   7   DODATKOWE:
Stwórz   aplikację   która   działa   jak   komenda   'find'.
Find   jest   komendą   którą   możemy   znaleźć   na   każdym   systemie   operacyjnym.   Zadaniem   tej komendy   jest   szukanie   plików.